Challenge 2: Deploy Two Apps (a Frontend and a Backend)
Time Limit: 15 minutes In this challenge, you will:

Deploy a frontend app
Deploy two different versions of a backend app
Use Jaeger to inspect the architecture
Deployment Overview
frontend: The web app that serves a UI to your visitors. It deconstructs complex requests and sends calls to numerous backend apps.
backend-v1: A business logic app that serves data to frontend via the Kubernetes API.

Part 1: Install the Backend App
Step 1: Create a Deployment
You'll use a YAML file to create a Deployment for backend-v1.

Use the Editor tab or the text editor of your choice.
*Hint:* If you use the Editor tab, be sure to click the save icon next to the file title.
Text Editor

File name:

1-backend-v1.yaml
File definition:

apiVersion: v1
kind: ConfigMap
metadata:
  name: backend-v1
data:
  nginx.conf: |-
    events {}
    http {
        server {
            listen 80;
            location / {
                return 200 '{"name":"backend","version":"1"}';
            }
        }
    }
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: backend-v1
spec:
  replicas: 1
  selector:
    matchLabels:
      app: backend
      version: "1"
  template:
    metadata:
      labels:
        app: backend
        version: "1"
      annotations:
    spec:
      containers:
        - name: backend-v1
          image: "nginx"
          ports:
            - containerPort: 80
          volumeMounts:
            - mountPath: /etc/nginx
              name: nginx-config
      volumes:
        - name: nginx-config
          configMap:
            name: backend-v1
---
apiVersion: v1
kind: Service
metadata:
  name: backend-svc
  labels:
    app: backend
spec:
  ports:
    - port: 80
      targetPort: 80
  selector:
    app: backend
Step 2: Deploy backend-v1
Use this command to deploy backend-v1:

kubectl apply -f 1-backend-v1.yaml
You should get this output:

configmap/backend-v1 created
deployment.apps/backend-v1 created
service/backend-svc created
Step 3: Verify Deployment
Verify backend-v1 is deployed with this command:

kubectl get pods,services
You should see this for the result:

NAME                              READY   STATUS
pod/backend-v1-745597b6f9-hvqht   2/2     Running

NAME                  TYPE        CLUSTER-IP       PORT(S)
service/backend-svc   ClusterIP   10.102.173.77    80/TCP
service/kubernetes    ClusterIP   10.96.0.1        443/TCP
You may be wondering, "Why are there two pods running for backend-v1?"
NGINX Service Mesh injects a sidecar proxy into your Pods. This sidecar container intercepts all incoming and outgoing traffic to your Pods. All the data collected is used for metrics, but you can also use this proxy to decide where the traffic should go.
The Kubernetes API server


Part 2: Deploy the Frontend App
Next you will deploy frontend app.

Step 1: Create the Deployment
As before, this involves creating a YAML file.
File name:

2-frontend.yaml
File definition - notice the pod uses cURL to issue a request to the backend service (backend-svc) every second:

apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
spec:
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        app: frontend
    spec:
      containers:
      - name: frontend
        image: curlimages/curl:7.72.0
        command: [ "/bin/sh", "-c", "--" ]
        args: [ "sleep 10; while true; do curl -s http://backend-svc/; sleep 1 && echo ' '; done" ]
Step 2: Deploy Frontend App
Use this command to deploy frontend:

kubectl apply -f 2-frontend.yaml
Expected result: deployment.apps/frontend created

Step 3: Confirm App Deployment
Run this command to confirm that frontend is running:

kubectl get pods
You should see the frontend and backend services - and again note there are also two pods for frontend since it includes a sidecar from NGINX Service Mesh:

NAME                         READY   STATUS    RESTARTS
backend-v1-5cdbf9586-s47kx   2/2     Running   0
"frontend-6c64d7446-mmgpv"     2/2     Running   0
Step 4: Check Logs
You will need to piece together the command for pulling logs that follows this format:
kubectl logs -c frontend <insert the full pod id displayed in your Terminal>

Add this command to your terminal:

kubectl logs -c frontend
Then add the frontend pod ID, which is unique for each lab user. Your command should look similar to this: kubectl logs -c frontend frontend-6c64d7446-mmgpv

TIP: SAVE THIS COMMAND Because you'll use this command repeatedly in this lab, it's useful to save it in a Text Editor file. We'll use this file name and reference it throughout the remainder of the lab:

frontend-pod-log-command.yaml
Now submit the command in your terminal!

When you get the logs, you should see that all the traffic is going to backend-v1 - {"name":"backend","version":"1"} - which is expected since it's your only backend.

Step 5: Inspect the Dependency Graph with Jaeger
What's more interesting is that the NGINX Service Mesh proxies deployed alongside the two apps are collecting metrics as the traffic flows. NGINX Service Mesh can use this data to derive a dependency graph of your architecture.

Open Jaeger tab to see the Jaeger dashboard that traces the backend dependency.

Click the "System Architecture" tab, where you should see a very simple architecture like this (hover your cursor over the graph to get the labels):

dashboard

Now imagine if you had dozens or even hundreds of backend services that will be accessed by frontend - this would be a very interesting graph!

Let's test that by creating a second backend service:


Part 3: Add A Second Backend App
Step 1: Create 2nd Backend Resource File
As before, this involves creating a YAML file.
File name:

3-backend-v2.yaml
File definition (below)

Notice how the deployment shares an app: backend label with the previous deployment.
Because the Service selector is app: backend, you should expect the traffic to be distributed evenly between backend-v1 and backend-v2.
apiVersion: v1
kind: ConfigMap
metadata:
  name: backend-v2
data:
  nginx.conf: |-
    events {}
    http {
        server {
            listen 80;
            location / {
                return 200 '{"name":"backend","version":"2"}';
            }
        }
    }
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: backend-v2
spec:
  replicas: 1
  selector:
    matchLabels:
      app: backend
      version: "2"
  template:
    metadata:
      labels:
        app: backend
        version: "2"
      annotations:
    spec:
      containers:
        - name: backend-v2
          image: "nginx"
          ports:
            - containerPort: 80
          volumeMounts:
            - mountPath: /etc/nginx
              name: nginx-config
      volumes:
        - name: nginx-config
          configMap:
            name: backend-v2
Step 2: Deploy Backend V2
Use this command to deploy backend-v2:

kubectl apply -f 3-backend-v2.yaml
You should get this result:

configmap/backend-v2 created
deployment.apps/backend-v2 created
Step 3: Inspect Logs
Use the command saved in frontend-pod-log-command.yaml to inspect your logs.

Now you should see responses from both backend versions - distributed evenly:

{"name":"backend","version":"1"}
{"name":"backend","version":"2"}
{"name":"backend","version":"1"}
{"name":"backend","version":"2"}
{"name":"backend","version":"1"}
{"name":"backend","version":"2"}
{"name":"backend","version":"1"}
Step 4: Check Jaeger
Return to the Jaeger tab to see if NGINX Service Mesh was able to correctly map both backend versions.

It can take a few minutes for Jaeger to recognize that a second backend was added.
If it doesn't show up after a minute, just continue to the next challenge - we promise it will become available!
dashboard

Observability and control are excellent reasons to use a service mesh. But you can do so much more.

For example, what if backend-v2 should only receive a fraction of the traffic? You'll find out in the last challenge.

To complete this challenge, click Check.