Challenge 1: Deploy a Cluster and NGINX Service Mesh
Time Limit: 6 minutes In this challenge, you will:

Deploy a minikube cluster
Install NGINX Service Mesh

Part 1: Create a Cluster
Step 1: Create the Cluster
Create your minikube cluster with the following command:

minikube start \
--extra-config=apiserver.service-account-signing-key-file=/var/lib/minikube/certs/sa.key \
  --extra-config=apiserver.service-account-key-file=/var/lib/minikube/certs/sa.pub \
  --extra-config=apiserver.service-account-issuer=kubernetes/serviceaccount \
  --extra-config=apiserver.service-account-api-audiences=api
Did you notice minikube command this looks different from previous labs?

There are some extra configs required to enable Service Account Token Volume Projection - necessary for NGINX Service Mesh.
With this feature enabled, the kubelet mounts a Service Account Token into each pod.
From this point, the pod can be identified uniquely in the cluster, and the kubelet takes care of rotating the token at a regular interval.
Optional reading: How the Service Account Token allows for identity verification in the cluster.

It may take a few seconds for the cluster to fully deploy. This will be your indicator that it's done:

🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
Step 2: Install NGINX Service Mesh
In this lab, you'll use NGINX Service Mesh - the free service mesh developed and maintained by F5 NGINX.

There are two options for installation:

Using Helm
Download and use the nginx-meshctl command-line utility
You will use Helm because it's the simplest and fastest method.

Note: The Helm chart is pre-loaded in this lab so you won't need the helm repo add step from in previous labs.
Use this command to install NGINX Service Mesh using Helm:

helm install nms ./nginx-service-mesh  --namespace nginx-mesh --create-namespace
It may take a few seconds to see the expected result:

NAME: nsm
NAMESPACE: nginx-mesh
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
NGINX Service Mesh has been installed. Ensure all NGINX Service Mesh Pods are in the Ready state before deploying your apps.
Verify that all pods are running with:

kubectl get pods --namespace nginx-mesh
It may take 1.5 minutes for all of the pods to deploy. You'll notice that, in addition to the NGINX Service Mesh pods, there are pods for Grafana, Jaeger, NATS, Prometheus, and Spire.

Grafana, Jaeger, and Prometheus are observability tools
NATS and Spire are used for identity purposes
Check out the docs for information on how these tools work with NGINX Service Mesh.

NAME                                  READY   STATUS
grafana-7c6c88b959-62r72              1/1     Running
jaeger-86b56bf686-gdjd8               1/1     Running
nats-server-6d7b6779fb-j8qbw          2/2     Running
nginx-mesh-api-7864df964-669s2        1/1     Running
nginx-mesh-metrics-559b6b7869-pr4pz   1/1     Running
prometheus-8d5fb5879-8xlnf            1/1     Running
spire-agent-9m95d                     1/1     Running
spire-server-0                        2/2     Running
To complete this challenge, click Check.