Challenge 3: Visualize and Generate Traffic
Time limit: 15 minutes In this challenge, you will:

Use Prometheus to get visibility into NGINX Ingress Controller performance
Use Locust to simulate a traffic surge
Observe the impact of increased traffic on NGINX Ingress Controller performance

Part 1: Explore Metrics
As you already discovered, an Ingress controller is a regular pod that bundles a reverse proxy (NGINX) with some code that integrates with Kubernetes. If your app will receive a lot of traffic, you'll want to scale the number of NGINX Ingress Controller pods and increase the replica count. To do this, you need metrics.

Step 1: Explore Available Metrics
NGINX Ingress Controller exposes multiple metrics. There are eight metrics for the NGINX Ingress Controller you're using in this lab (based on NGINX Open Source) and 80+ metrics for the option based on NGINX Plus.

Step 2: Create a Temporary Pod
First you need the IP address of the NGINX Ingress Controller pod. Get it using the command:
kubectl get pods -o wide
You will see the internal IP address of the NGINX Ingress Controller pod in the output (172.17.0.4 in this case)

NAME                                  READY   STATUS    RESTARTS   AGE     "IP"           NODE       NOMINATED NODE   READINESS GATES
main-nginx-ingress-779b74bb8b-6hdwx   1/1     Running   0          3m43s   "172.17.0.4"   minikube   <none>           <none>
podinfo-5d76864686-nl8ws              1/1     Running   0          5m49s   172.17.0.3     minikube   <none>           <none>
Now you can create the temporary pod with this command:

kubectl run -ti --rm=true busybox --image=busybox
You will get a shell on a machine inside the Kubernetes cluster.

If you don't see a command prompt, try pressing enter.
/ #
Step 3: View Available Metrics

Use the following command to retrieve a list of the available metrics (note that it includes the IP address you found earlier):
wget -qO- 172.17.0.4:9113/metrics
You'll get the list of metrics provided by this version of NGINX Ingress Controller - but which one should you use?


For scaling purposes, nginx_connections_active is ideal because it keeps track of the number of requests being actively processed, which will help you identify when a pod needs to scale.

Type exit at the command prompt of the temporary pod to get back to the Kubernetes server.


Part 2: Install Prometheus
To trigger autoscaling based on nginx_connections_active, you need two tools:

A mechanism to scrape the metrics - we'll use Prometheus
A tool to store and expose the metrics so that Kubernetes can use them - we'll use KEDA.
Prometheus is a popular open source project of the Cloud Native Computing Foundation (CNCF) for monitoring and alerting. NGINX Ingress Controller offers Prometheus metrics that are useful for visualization and troubleshooting.

Step 1: Install Prometheus
The fastest way to install Prometheus is with Helm, which is already installed on this host.
Add the Prometheus repository to Helm using this command:

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
Next, download and install Prometheus:

  helm install prometheus prometheus-community/prometheus \
  --set server.service.type=NodePort --set server.service.nodePort=30010
Step 2: Verify Prometheus is Installed
Run this command to confirm that Prometheus is installed and running:
kubectl get pods
Prometheus isn't completely installed yet! All pods must be running before you can start using Prometheus - full installation usually takes 30-60 seconds to complete.

NAME                                             READY   STATUS              RESTARTS   AGE
main-nginx-ingress-779b74bb8b-mtdkr              1/1     Running             0          3m23s
podinfo-5d76864686-fjncl                         1/1     Running             0          5m41s
prometheus-alertmanager-d6d94cf4b-85ww5          0/2     ContainerCreating   0          7s
prometheus-kube-state-metrics-7cd8f95c7b-86hhs   0/1     Running             0          7s
prometheus-node-exporter-gqxfz                   1/1     Running             0          7s
prometheus-pushgateway-56745d8d8b-qnwcb          0/1     ContainerCreating   0          7s
prometheus-server-b78c9449f-kwhzp                0/2     ContainerCreating   0          7s
Step 3: Query Prometheus
Switch to the "Prometheus" tab to view the dashboard. As before, you will need to use the refresh icon. If you came straight here after the last step, be prepared to wait up to 30 seconds before the dashboard loads.

Now that Prometheus is available, search for the active connections metric in the search bar:

nginx_ingress_nginx_connections_active
You will see one active connection, which makes sense because you've deployed one NGINX Ingress Controller pod.


Part 3: Install Locust
Next you will use Locust to simulate a traffic surge that you can detect with the Prometheus dashboard.

Step 1: Create a Locust Pod
You need a YAML file to create a pod for the load generator. You can use the Editor tab or the text editor of your choice.

File name:

3-locust.yaml
File Definition:

apiVersion: v1
kind: ConfigMap
metadata:
  name: locust-script
data:
  locustfile.py: |-
    from locust import HttpUser, task, between

    class QuickstartUser(HttpUser):
        wait_time = between(0.7, 1.3)

        @task
        def hello_world(self):
            self.client.get("/", headers={"Host": "example.com"})
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: locust
spec:
  selector:
    matchLabels:
      app: locust
  template:
    metadata:
      labels:
        app: locust
    spec:
      containers:
        - name: locust
          image: locustio/locust
          ports:
            - containerPort: 8089
          volumeMounts:
            - mountPath: /home/locust
              name: locust-script
      volumes:
        - name: locust-script
          configMap:
            name: locust-script
---
apiVersion: v1
kind: Service
metadata:
  name: locust
spec:
  ports:
    - port: 8089
      targetPort: 8089
      nodePort: 30015
  selector:
    app: locust
  type: LoadBalancer
Locust reads the following locustfile.py, which is stored in a ConfigMap. The script issues a request to the pod with the correct headers.

from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    wait_time = between(0.7, 1.3)

    @task
    def hello_world(self):
        self.client.get("/", headers={"Host": "example.com"})
Step 2: Submit the YAML File
To submit the YAML file, use the following command:
kubectl apply -f  3-locust.yaml
If successful, you will see:

configmap/locust-script created
deployment.apps/locust created
service/locust created
Step 3: Use Locust to Scale Traffic
Go to the "Locust" tab and click refresh. You'll see a welcome page with the option to change number of users, spawn rate, and host.
Welcome page for the Locust load testing tool

To simulate a traffic surge, enter the following details into Locust:

Number of users: 1000
Spawn rate: 10
Host: http://main-nginx-ingress
Click "Start swarming" and observe the traffic reaching NGINX Ingress Controller.

Step 4: Return to Prometheus
Now that you have traffic routing through NGINX Ingress Controller to your Podinfo app, you can return to Prometheus to see how the Ingress controller responds.
As a vast amount of connections are issued, the single Ingress controller pod struggles to process the increased traffic without latency.
Through observing where performance degrades, you might notice that 100 active connections is a tipping point for latency issues. This tipping point may be lower depending on your organization's tolerance for latency.
Once you determine an ideal active threshold for active connections (example: 100), then you can use that information to determine when scale NGINX Ingress Controller.
Plotting active connection on NGINX with Prometheus

To complete the challenge, press Check.